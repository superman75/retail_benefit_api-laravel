<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;  
use Validator, DB, Mail;
use Illuminate\Support\Facades\Password;

class AuthController extends Controller
{
    //
    /**
     * API Register
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function signup(Request $request)
    {

        $credentials = $request->only('email', 'password');
        
        $rules1 = [
            'email' => 'required|email|max:255',
        ];
        $rules2 = [
            'password' => 'required|max:255|min:6'
        ];
        $rules3 = [
            'email' => 'required|unique:users',
        ];
        $error = [];
        
        $validator1 = Validator::make($credentials, $rules1);
        $validator2 = Validator::make($credentials, $rules2);
        $validator3 = Validator::make($credentials, $rules3);
        if($validator1->fails())$error["email"] = "must be a valid format";
        if($validator2->fails())$error["password"] = "must be over 6 characters";
        if($validator3->fails())$error["email"] = "is already registered by other customer.";
        if($error){
            return response()->json(['errors' => $error]);
        }
        // if($validator1->fails() && $validator2->fails()) {
        //     return response()->json(['errors' => ['email' => 'must be a valid format.', 'password'=>'must be between 5 and 8 characters']]);
        // }
        // if($validator1->fails()){
        //     return response()->json(['errors' => ['email' => 'must be a valid format.']]);
        // }
        // if($validator2->fails()){
        //     return response()->json(['errors' => ['password'=>'must be between 5 and 8 characters']]);
        // }
        $email = $request->email;
        $password = $request->password;
        $login = $request->login;
        $first_name = $request->first_name;
        $last_name = $request->last_name;
        $custom_identifier = $request->custom_identifier;
        $custom_referrer_identifier = $request->custom_referrer_identifier;
        if($login == null)$login = $email;
        
        $user = User::create(['name' => '', 'login' => $login, 'email' => $email, 'password' => bcrypt($password), 'first_name' => $first_name, 'last_name' => $last_name, 'custom_identifier' => $custom_identifier, 
            'custom_referrer_identifier' => $custom_referrer_identifier, 'custom_role' => 'member', 'custom_parent_identifier' => 'D1']);

        //DB::table('users')->insert(['user_id'=>$user->id]);

        return response()->json(['first_name'=>$first_name , 'last_name' => $last_name, 'email' => $email, 'custom_identifier' => $user->id, 'custom_parent_identifier' => 'D1', 'custom_referrer_identifier' => $custom_referrer_identifier,
        	'custom_role' => 'member']);
    }
    /**
     * API Login, on success return JWT Auth token
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function signin(Request $request)
    {
        $credentials = $request->only('login', 'password');
        
        $rules = [
            'login' => 'required|unique:users',
        ];
        $error = [];
        
        $validator = Validator::make($credentials, $rules);
        if(!$validator->fails())$error["login"] = "not found in system";
        if($error){
            return response()->json(['errors' => $error]);
        }              
        try {
            // attempt to verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['errors' => ['password'=>'not matched']]);
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['errors' => ['server' => 'error']]);
        }
        // all good so return the token
        $currentUser = JWTAuth::user();
        return response()->json(['first_name'=>$currentUser->first_name , 'last_name' => $currentUser->last_name,   'email' => $currentUser->email, 'custom_identifier' => $currentUser->id,                 'custom_parent_identifier' => $currentUser->custom_parent_identifier, 'custom_referrer_identifier' => $currentUser->custom_referrer_identifier,
            'custom_role' => $currentUser->custom_role]);
    }


    /**
     * API Recover Password
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function reset(Request $request)
    {
        $credentials = $request->only('email', 'password','password_confirmation','custom_identifier');
        
        $rules1 = [
            'email' => 'required|email|max:255',
        ];
        $rules2 = [
            'password' => 'required|max:255|min:6'
        ];
        $error = [];
        
        $validator1 = Validator::make($credentials, $rules1);
        $validator2 = Validator::make($credentials, $rules2);

        if($validator1->fails())$error["email"] = "must be a valid format";
        if($validator2->fails())$error["password"] = "must be over 6 characters"; 

        if ($request->password_confirmation != $request->password) {
             $error["password_confirmation"] = "should be same to password";
        }
        if($error){
            return response()->json(['errors' => $error]);
        }
        $user = User::where('email', $request->email)->first();
        if(!$user){
             return response()->json(['errors' => ['email' => 'not found in system']]);
        }
        
        if ($user->id != $request->custom_identifier){
            return response()->json(['errors' => ['custom_identifier' => 'not matched']]);
        }
        $user->password = bcrypt($request->password);
        $user->save();
    
        return response()->json([
            'custom_identifier' => $user->id
        ]);
    }
}
